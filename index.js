const http = require('http')
const fs = require("fs");
const uuid = require('uuid');

const port = 8000;

const json = {
    "slideshow": {
        "author": "Yours Truly",
        "date": "date of publication",
        "slides": [
            {
                "title": "Wake up to WonderWidgets!",
                "type": "all"
            },
            {
                "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                ],
                "title": "Overview",
                "type": "all"
            }
        ],
        "title": "Sample Slide Show"
    }
}


function uuidGenerator() {
    let generatedUuid = uuid.v4();
    return (generatedUuid);
}

const server = http.createServer((req, res) => {

    if (req.url.startsWith("/status/")) {

        const statusCode = parseInt(req.url.split("/")[2]);
        console.log("Parsed status code:", statusCode);

        if (statusCode >= 100 && statusCode <= 599) {

            res.writeHead(statusCode, { "Content-Type": "text/plain" });
            res.end(String(statusCode));

        } else {

            res.writeHead(400, { "Content-Type": "text/plain" });
            res.end("Invalid status code");

        }
    } else if (req.url.startsWith("/delay/")) {

        const delay = parseInt(req.url.split("/")[2]);
        console.log("Parsed delay:", delay);

        setTimeout(() => {

            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end(`Delayed response by : ${delay} seconds`);

        }, delay * 1000)

    } else if (req.url == "/html") {

        fs.readFile("./index.html", (err, data) => {

            if (err) {
                res.writeHead(500, { 'Content-Type': 'text/plain' });
                res.end("Error : ", err);
            } else {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.end(data);
            }

        })

    } else if (req.url == "/json") {

        const jsonData = JSON.stringify(json);
        res.writeHead(200, { "content-Type": "application/json" })
        res.end(jsonData);

    } else if (req.url == "/uuid") {

        const uuidData = uuidGenerator();
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(uuidData);

    } else {

        res.writeHead(404, { "Content-Type": "text/plain" });
        res.end("Invalid Request");

    }

})

server.listen(port, () => {
    console.log(port);
})